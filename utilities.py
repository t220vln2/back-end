import os

def parse_and_load_environment_variables(filename, delimiter='='):
    if os.path.exists(filename):
        for line in open(filename):
            var = line.strip().split(delimiter)
            if len(var) == 2:
                os.environ[var[0]] = var[1]
