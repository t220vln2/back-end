# Back-end #

## Installation ##
*python 3.4*

`$ python3.4 -m venv venv`

`$ source venv/bin/activate`

(venv) `$ pip install -r requirements.txt`

## Run ##

`python manage.py`
