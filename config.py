import os
from utilities import parse_and_load_environment_variables

# constant for relative path
basedir = os.path.abspath(os.path.dirname(__file__))

# configure environment variables
parse_and_load_environment_variables(os.path.join(basedir, '.env'))

class Config:

    SECRET_KEY = os.environ.get('SECRET_KEY') or 'hard-coded secret'
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    USE_TOKEN_AUTH = True

    # Storage settings
    STORAGE_PROVIDER = 'LOCAL'
    STORAGE_CONTAINER = '/tmp'
    STORAGE_KEY = ''
    STORAGE_SECRET = ''
    STORAGE_SERVER = True

    # Mail settings
    MAIL_SERVER   = 'smpt.googlemail.com'
    MAIL_PORT     = 587
    MAIL_USE_TLS  = True
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')

    BACK_END_ADMIN               = os.environ.get('BACK_END_ADMIN')
    BACK_END_MAIL_SENDER         = 'Launagreining Admin <birkiro13@gmail.com>'
    BACK_END_MAIL_SUBJECT_PREFIX = '[Launagreining]'

    @staticmethod
    def init_app(app):
        pass

class DevelopmentConfig(Config):
    DEBUG = True
    USE_TOKEN_AUTH = False
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
            'sqlite:///' + os.path.join(basedir, 'data-dev.sqlite')

class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or \
            'sqlite:///' + os.path.join(basedir, 'data-test.sqlite')

class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
            'sqlite:///' + os.path.join(basedir, 'data.sqlite')

config = {
    'default'     : DevelopmentConfig,

    'development' : DevelopmentConfig,
    'testing'     : TestingConfig,
    'production'  : ProductionConfig
}
