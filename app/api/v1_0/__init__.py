from flask import Blueprint, g, jsonify
from ..authentication import auth
from ..errors import forbidden, unauthorized, bad_request, not_found
from ...exceptions import ValidationError

api = Blueprint('api', __name__)


@api.before_request
@auth.login_required
def before_request():
    if not g.current_user.confirmed:
        return forbidden('Unconfirmed account')
    else:
        g.current_user.ping()
        pass

@api.after_request
def after_request(response):
    if hasattr(g, 'headers'):
        response.headers.extend(g.headers)
    return response

@api.errorhandler(ValidationError)
def validation_error(e):
    return bad_request(e.args[0])

@api.errorhandler(404)
def page_not_found(e):
    return not_found(e.args[0])

from . import users
