from flask import jsonify, request, current_app, url_for
from . import api
from ...models import User



@api.route('/users/', methods=['GET'])
def get_users():
    users = User.query
    return jsonify({'message': 'random users'})

@api.route('/users/<int:id>', methods=['GET'])
def get_user(id):
    user = User.query.get_or_404(id)
    return jsonify(user.to_json)

@api.route('/users/', methods=['POST'])
def insert_user():
    return jsonify({'message': 'random message'})
