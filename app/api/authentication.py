from flask import g, current_app
from flask.ext.httpauth import HTTPBasicAuth
from ..models import User
from .errors import unauthorized


auth = HTTPBasicAuth()


@auth.verify_password
def verify_password(username_or_token, password):
    if current_app.config['USE_TOKEN_AUTH']:
        g.current_user = User.verify_auth_token(username_or_token)
        g.token_used = True
        return g.current_user is not None
    else:
        g.current_user = User.query.filter_by(username=username_or_token).first()
        return g.current_user is not None and g.current_user.verify_password(password)


@auth.error_handler
def auth_error():
    return unauthorized('Invalid Credentials')
