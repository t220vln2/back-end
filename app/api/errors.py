from flask import jsonify, url_for, current_app

# status codes 400, 401, 403, 404, 405, 412, 429, 500

def bad_request(message):
    res = jsonify({'status': 400, 'error': 'bad request', 'message': message})
    res.status_code = 400
    return res

def unauthorized(message=None):
    if message is None:
        if current_app.config['USE_TOKEN_AUTH']:
            message = 'please authenticate with your token'
        else:
            message = 'please authenticate'
    res = jsonify({'status': 401, 'error': 'unauthorized', 'message': message})
    res.status_code = 401
    if current_app.config['USE_TOKEN_AUTH']:
        res.headers['Location'] = url_for('token.request_token')
    return res

def forbidden(message):
    res = jsonify({'status': 403, 'error': 'forbidden', 'message': message})
    res.status_code = 403
    return res

def not_found(message):
    res = jsonify({'status': 404, 'error': 'not found', 'message': message})
    res.status_code = 404
    return res

def method_not_allowed():
    res = jsonify({'status': 405, 'error': 'method not allowed'})
    res.status_code = 405
    return res

def precondition_failed(message=None):
    res = jsonify({'status': 412, 'error': 'precondition failed', 'message': message})
    res.status_code = 412
    return res

def too_many_requests(message='You have exceeded your request rate'):
    res = jsonify({'status': 429, 'error': 'too many requests', 'message': message})
    res.status_code = 429
    return res

def internal_server_error(message=''):
    res = jsonify({'status': 500, 'error': 'internal server error', 'message': message})
    res.status_code = 500
    return res
