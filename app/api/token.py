from flask import current_app, Blueprint
from flask.ext.httpauth import HTTPBasicAuth

token = Blueprint('token', __name__)
token_auth = HTTPBasicAuth()



@token_auth.verify_password
def verify_password(username, password):
    g.user = User.query.filter_by(username=username).first()
    if not g.user:
        return False
    return g.user.verify_password(password)


@token_auth.error_handler
def unauthorized_error():
    return unauthorized('Please authenticate to get your token.')


@token.route('/token', methods=['POST'])
@token_auth.login_required
def request_token():
    return {'token': g.user.generate_auth_token(expires_in=86400 + ':')}
