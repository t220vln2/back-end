from flask import Flask
from flask.ext.mail import Mail
from flask_cloudy import Storage
from .api.authentication import auth
from .models import db
from config import config



# storing files
mail = Mail()
storage = Storage()


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    db.init_app(app)
    mail.init_app(app)
    storage.init_app(app)

    #if not app.debug and not app.testing and not app.config['SSL_DISABLE']:
        #from flask.ext.sslify import SSLify
        #sslify = SSLify(app)

    from .api.v1_0 import api as api_1_0_blueprint
    app.register_blueprint(api_1_0_blueprint, url_prefix='/v1.0')

    from .api.token import token as token_blueprint
    app.register_blueprint(token_blueprint, url_prefix='/auth')

    @app.errorhandler(404)
    @auth.login_required
    def not_found_error(e):
        return not_found('item not found')

    @app.route('/upload', methods=['POST'])
    def upload():
        from flask import request, jsonify
        from werkzeug import secure_filename
        from datetime import date

        try: 
            file = request.files.get('file')
            my_object = storage.upload(file, prefix='/username/' + date.today().isoformat())
            return jsonify({'object_name': my_object.name})
        except Exception as e:
            return jsonify({'error': e.args[0]}), 500

    return app
