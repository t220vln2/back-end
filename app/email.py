from flask import current_app, render_template
from flask.ext.mail import Message
from threading import Thread
from . import mail


def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)


def send_email(to, subject, **kwargs):
    app = current_app.__get_current_object()
    msg = Message(app.config['BACK_END_MAIL_SUBJECT_PREFIX'] + ' ' + subject,
            sender=app.config['BACK_END_MAIL_SENDER'], recipients=[to])
    msg.body = render_template(template + '.txt', **kwargs)
    msg.html = render_template(template + '.html', **kwargs)
    runner = Thread(target=send_async_mail, args=[app, msg])
    runner.start()
    return runner
