import unittest
import json
from flask import current_app, url_for
from app import create_app, db
from app.models import User, Role

from base64 import b64encode


class APITestCase(unittest.TestCase):
    """This class is for testing API"""

    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        Role.insert_roles()
        self.client = self.app.test_client()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    ##################################################

    def get_api_headers(self, username, password):
        return {
            'Authorization': 'Basic ' + b64encode(
                (username + ':' + password).encode('utf-8')).decode('utf-8'),
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }

    def test_no_auth(self):
        response = self.client.get(url_for('api.get_users'), content_type='application/json')
        self.assertTrue(response.status_code == 401)


    def test_bad_auth(self):
        # add a user
        r = Role.query.filter_by(name='User').first()
        self.assertIsNotNone(r)
        u = User(email='tester@test.com', password='avocado', confirmed=True, role=r)
        db.session.add(u)
        db.session.commit()

        # authenticate with bad password
        response = self.client.get(url_for('api.get_users'),
                headers=self.get_api_headers('tester@test.com', 'nothing'))
        self.assertTrue(response.status_code == 401)
